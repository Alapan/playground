#!/usr/bin/python3.4
import time
import calendar
import urllib.request
import concurrent.futures
import json

counter = 100
params = {}
url = []
list = []

def set_params():
    
    '''Set query parameters for requests'''

    params['start'] = calendar.timegm(time.gmtime())+10
    params["email"] = 'abc'

    for i in range(counter):
        params['id'] = i
        params['qid'] = 1 if i%2==0 else 2
        query_params = urllib.parse.urlencode(params)
        url.append("https://localhost/taketicket.php" + "?" + query_params)
    
    return url

def sendRequest(url):

    '''Send HTTP requests'''

    response = urllib.request.urlopen(url)
    json_obj = json.loads(response.read().decode('utf-8'))
    #add to global list for display
    list.append(json_obj)
    
    # add the return to a dict
    # after all the requests are sent and received
    # dict[<queue-id>] = {ticket#, pid#, start_time}
    # foreach item in dict[queue-id], ticket# is unique, pid# is unique, start_time is same

def addResponse(list):

    '''add received responses to dict'''

    queue_dict = {}
    value_one = []
    value_two = []

    for item in list:
        if item['queue'] == '1':
            value_one.append({'ticket number':item['ticket'],'pid number':item['pid'],'start time':item['start_time']})
        if item['queue'] == '2':
            value_two.append({'ticket number':item['ticket'],'pid number':item['pid'],'start time':item['start_time']})

    queue_dict['1'] = value_one
    queue_dict['2'] = value_two 

    testResponse(queue_dict)    

 
def testResponse(queue_dict):

    '''Test received responses'''

    for key,values in queue_dict.items():
        tickets = []
        pids = []
        start_times = []
        for v in values:
            tickets.append(v['ticket number'])
            pids.append(v['pid number'])
            start_times.append(v['start time'])
        print(' ')
        print('Checking for unique ticket numbers...')

        # Convert each list to set. If elements are unique, len(set)=len(list), else they are unequal
        if len(set(tickets)) == len(tickets):
            print('Test for unique ticket numbers for queue ' + key + ' : PASS')
        else:
            print('Test for unique ticket numbers for queue ' + key + ' : FAIL')

        print('Checking for unique process IDs...')
        if len(set(pids)) == len(pids):
            print('Test for unique process ID for queue ' + key + ' : PASS')
        else:
            print('Test for unique process ID for queue ' + key + ' : FAIL')

        
        print('Checking for same start times...')
        if len(set(start_times)) != len(start_times):
            print('Test for same start time for queue ' + key + ' : PASS')
        else:
            print('Test for same start time for each queue ' + key + ' : FAIL')

def main():

    url_array = set_params()
    
    with concurrent.futures.ThreadPoolExecutor(max_workers=counter) as pool:
        
        for url in url_array:           
            pool.submit(sendRequest, url)

    addResponse(list)

if __name__ == '__main__':
    main()
